/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/*================================================================================
Name:          main.c
Author:        Roman Grintsevich
Compile with:  trueSTUDIO
Date:          2017-02-19
Description:   Project - Game snake
Picture 1:	   https://dl.dropboxusercontent.com/u/13134347/Kom%26opt_ProjectGameMenu.JPG
Picture 2:	   https://dl.dropboxusercontent.com/u/13134347/Kom%26opt_Titel.JPG
Video 1x:	   https://dl.dropboxusercontent.com/u/13134347/Kom%26opt_Project.mp4
================================================================================*/

//--OLED-start-//
#include "font.h"
//--OLED-end-//

//--SNAKE-start-//
#include "snake.h"
//--SNAKE-end-//

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

//--COMMON-START-//
#define CDELAY				cdelay();
#define HIGH				GPIO_PIN_SET
#define LOW					GPIO_PIN_RESET
//--COMMON-END//

//--LED & KEYS-START-//
#define STB(x)				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, x)			//A3
#define CLK(x)				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, x)			//A4
#define DIO(x)				HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, x)			//A5
#define rDIO				HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)				//A5
#define LK_ADDRESS			0xc0	//+ address
#define LK_DATA				0x40
#define LK_DISPLAY			0x80	//+ intensity
#define	LK_ON				0x08
#define KEYSCAN				0x42	//Key scan command
#define ledBinaryMask 		0x01 //Mask out one bit
const uint8_t decimalOnSevenSegment[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};
uint8_t LK_points[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t LK_Life[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint32_t points = 0; //Max value 4 294 967 295
//--LED & KEYS-END-//

//--OLED-START-//
#define SCL(x)				HAL_GPIO_WritePin(SCL_GPIO_Port, SCL_Pin, x)
#define SDA(x)				HAL_GPIO_WritePin(SDA_GPIO_Port, SDA_Pin, x)
#define rSDA				HAL_GPIO_ReadPin(SDA_GPIO_Port, SDA_Pin)
#define OLED_ADDRESS		(0x3c<<1)
#define OLED_WRITE			0
#define OLED_READ			1
#define OLED_HORIZONTAL		0
#define OLED_VERTICAL		1
#define OLED_COMMAND		0x00
#define OLED_DATA			0x40
#define OLED_STARTBIT		SDA(LOW);  CDELAY	//SCK=HIGH
#define OLED_STOPBIT		SCL(HIGH); CDELAY; SDA(HIGH); CDELAY; CDELAY;	//SCK=HIGH
#define OLED_CONTRAST		0x81	//A
#define OLED_DISPLAYON(x)	(0xa4+x)
#define OLED_INVERSE(x)		(0xa6+x)
#define OLED_ONOFF(x)		(0xae +x)

#define OLED_LOCOL(x)		(x)
#define OLED_HICOL(x)		(0x10+x)
#define OLED_ADDRESSING		0x20	//A
#define OLED_SETCOL			0x21	//AB
#define OLED_SETPAGE			0x22	//AB
#define OLED_SETPAGESTART(x)	(0xb0+x)

#define OLED_STARTLINE(x)	(0x40+x)
#define OLED_REMAP(x)		(0xa0+x)
#define OLED_MULTIPLEX		0xa8	//A
#define OLED_OUTDIR(x)		(0xc0+(x<<3))
#define OLED_OFFSET			0xd3	//A
#define OLED_COMPINS		0xda	//A

#define OLED_DIVIDEFREQ		0xd5	//A
#define OLED_PRECHARGE		0xd9	//A
#define OLED_VCOMH			0xdb	//A
#define NOP					0xe3

#define OLED_CHARGEPUMP		0x8d	//A

#define OLED_DISABLESCROLL	0x2e
//--OLED-END-//

//--SNAKE-START-//
//Snake body
#define startPosElement		517
uint32_t bodyLenth = 4;
uint16_t headPosition = startPosElement; //##
uint16_t tailPosition = startPosElement-4; //##
uint8_t difficulty[4] = {100, 66, 34, 2}; //0 - easy, 1 - medium, 2 - hard, 3 - ultra hard
uint8_t gameDifficulty = 0;

//Food
uint16_t foodPosition = 600;
uint8_t	foodOnScreen = 0x01;
uint16_t foodCounter = 0;

//Move direction
#define RIGHT				1
#define LEFT				2
#define UP					64
#define DOWN				128
uint8_t moveDirection = 0;
uint8_t moveBy = 0x01;
uint8_t verticalMove = 0;

//Difficulties
#define EASY				0
#define MEDIUM				1
#define HARD				2
#define ULTRA				3
//--SNAKE-END-//

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

//--COMMON-START-//
void udelay(volatile unsigned int delay);
void cdelay(void);
//--COMMON-END-//

//--LED & KEYS-START-//
void init_LedAndKeys(void);
void LK_sendbyte(uint8_t data);
void LK_clearDisplay(void);
void LK_displayPoints(uint32_t pointsToDisplay);
uint8_t LK_decodeButtonData(uint32_t value);
uint32_t LK_getButtonData(void);
void LK_readButtons(void);
//--LED & KEYS-END-//


//--OLED-START-//
GPIO_PinState OLED_sendbyte(uint8_t data);
void OLED_printstr(char *string);
void OLED_clearDisplay(void);
void init_Oled(void);
//--OLED-END-//

//--SNAKE-START-//
void initGame(void);
void drawGame(void);
void updateMap(void);
void startPosition(void);
void updateMove(void);
void downMove(void);
void upMove(void);
void leftMove(void);
void rightMove(void);
void checkIfFood(void);
void addToSnake(void);
void checkOutOfFrame(void);
void addNewFood(void);
void setGameDiff(void);
//--SNAKE-END-//

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();

	/* USER CODE BEGIN 2 */
	//--LED & KEYS-START-//
	//Idle and clear Led and keys
	init_LedAndKeys();
	//--LED & KEYS-NED-//

	//--OLED-START-//
	init_Oled();
	//--OLED-NED-//

	//--SNAKE-START-//
	OLED_clearDisplay();
	HAL_Delay(250);
	initGame();
	//Set start direction
	moveDirection = 1;
	setGameDiff(); //Player will pick game difficulty from 0 - 4 (Easy, medium, hard, ultra).
	//--SNAKE-END-//

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */

	//--OLED-START-// ## DEBUG



	while (1)
	{
		//--SNAKE-START-//
		LK_readButtons();
		drawGame();
		checkIfFood();
		checkOutOfFrame();
		HAL_Delay(gameDifficulty); //Difficulty will change the speed of the snake move
		//--SNAKE-END-//

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 16;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}

}

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : DIO_Pin */
	GPIO_InitStruct.Pin = DIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(DIO_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : CLK_Pin */
	GPIO_InitStruct.Pin = CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(CLK_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : LD2_Pin SCL_Pin */
	GPIO_InitStruct.Pin = LD2_Pin|SCL_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pin : STB_Pin */
	GPIO_InitStruct.Pin = STB_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(STB_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : SDA_Pin */
	GPIO_InitStruct.Pin = SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SDA_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, DIO_Pin|CLK_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, LD2_Pin|SCL_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, STB_Pin|SDA_Pin, GPIO_PIN_RESET);

}

/* USER CODE BEGIN 4 */

//--COMMON-START-//

void cdelay(void)
{
	volatile int delay;
	for (delay=8;delay!=0;delay--);
}

void udelay(volatile unsigned int delay)
{
	delay*=11.206;
	for (;delay!=0;delay--);
}

//--COMMON-END-//

//--LED & KEY-START-//

void init_LedAndKeys(void)
{
	STB(HIGH);
	DIO(HIGH);
	CLK(HIGH);
	CDELAY;
	LK_clearDisplay();
}

void LK_sendbyte(uint8_t data)
{
	uint8_t i;

	//Start communication with TM1638
	STB(LOW); //Pull STB low to transfer data
	CDELAY;

	for(i=0x1;i!=0;i<<=1)
	{
		DIO(data & i);
		CLK(LOW);
		CDELAY;
		CLK(HIGH);
		CDELAY;
	}
}

//Function to clear display
void LK_clearDisplay(void)
{
	int i;

	//Reset address
	STB(LOW); //Pull STB low to transfer data
	CDELAY;		//
	LK_sendbyte(LK_ADDRESS); //Address command set
	CDELAY;

	//Reset display & LED memory values
	for(i = 0; i < 16; i++)
	{
		LK_sendbyte(0x00);
	}
	CDELAY;
	STB(HIGH); //Idle mode
	CDELAY;

	//Turn display on
	STB(LOW);
	CDELAY;
	LK_sendbyte(LK_DISPLAY+LK_ON);
	CDELAY;
	STB(HIGH); //Idle mode
	CDELAY;

	//Reset address
	STB(LOW); //Pull STB low to transfer data
	CDELAY;
	LK_sendbyte(LK_ADDRESS);
	CDELAY;
	DIO(HIGH);
	STB(HIGH); //Idle mode
	CDELAY;
}

//Function that will display points on the 7 segments on the TM1638
void LK_displayPoints(uint32_t pointsToDisplay)
{
	uint8_t i;

	//Convert from points decimal to 7 segment data for TM1538
	LK_points[7] = decimalOnSevenSegment[pointsToDisplay % 10];
	LK_points[6] = decimalOnSevenSegment[pointsToDisplay / 10 % 10];
	LK_points[5] = decimalOnSevenSegment[pointsToDisplay / 100 % 10];
	LK_points[4] = decimalOnSevenSegment[pointsToDisplay / 1000 % 10];
	LK_points[3] = decimalOnSevenSegment[pointsToDisplay / 10000 % 10];
	LK_points[2] = decimalOnSevenSegment[pointsToDisplay / 10000 % 10];
	LK_points[1] = decimalOnSevenSegment[pointsToDisplay / 100000 % 10];
	LK_points[0] = decimalOnSevenSegment[pointsToDisplay / 1000000 % 10];

	//Convert from game difficulty decimal to red LEDs data on the TM1538 {100, 66, 34, 2}
	if(gameDifficulty == 100)
	{
		LK_Life[0] = 0x01;
		LK_Life[1] = 0x00;
		LK_Life[2] = 0x00;
		LK_Life[3] = 0x00;
	}
	else if(gameDifficulty == 66)
	{
		LK_Life[0] = 0x01;
		LK_Life[1] = 0x01;
		LK_Life[2] = 0x00;
		LK_Life[3] = 0x00;
	}
	else if(gameDifficulty == 34)
	{
		LK_Life[0] = 0x01;
		LK_Life[1] = 0x01;
		LK_Life[2] = 0x01;
		LK_Life[3] = 0x00;
	}
	else if(gameDifficulty == 2)
	{
		LK_Life[0] = 0x01;
		LK_Life[1] = 0x01;
		LK_Life[2] = 0x01;
		LK_Life[3] = 0x01;
	}
	else
	{

	}

	//Clear display before showing new values on the 7 segment
	LK_clearDisplay();

	for(i = 0; i < 8; i++)
	{
		LK_sendbyte(LK_Life[i]); //Led data, will display life
		LK_sendbyte(LK_points[i]); //7 segment data, will display points
	}
}

//Function gets data with scanned keys from TM1638
uint32_t LK_getButtonData(void)
{
	uint32_t buttonValues = 0; //Variable will hold data with scanned keys from the TM1638
	uint32_t i;

	//Restart communication to send a command
	DIO(HIGH);	//Idle mode
	STB(HIGH);
	CDELAY;

	//Send command to scan the keys
	STB(LOW); //Pull STB low to transfer data/command
	CDELAY;	//Wait before STB is stable
	LK_sendbyte(KEYSCAN);	//Send key scan command
	udelay(1);	//1 us delay be
	DIO(HIGH);	//Release DIO for TM1638 to be able sending the data

	//Get 32 bits from the TM1638 (4 bytes)
	for(i = 0; i < 32; i++)
	{
		CLK(LOW);	//Pull CLK low to read bit on DIO
		CDELAY;		//Delay to get stable DIO
		buttonValues |= rDIO << i; //Read DIO for key values
		CLK(HIGH);	//Pull high CLK, TM1638 puts one bit on the DIO
		CDELAY;
	}

	//IDLE mode
	CDELAY;
	DIO(HIGH);	//Release data line
	STB(HIGH); //End transmission
	CDELAY;

	return buttonValues;
}

//Decode button data from the TM1638 to one byte
uint8_t LK_decodeButtonData(uint32_t value)
{
	return
			(value & 0x00000001)<<7  |
			(value & 0x00000100)>>2  |
			(value & 0x00010000)>>11 |
			(value & 0x01000000)>>20 |
			(value & 0x00000010)>>1  |
			(value & 0x00001000)>>10 |
			(value & 0x00100000)>>19 |
			(value & 0x10000000)>>28;
}

void LK_readButtons(void)
{
	uint8_t buttonPressed = LK_decodeButtonData(LK_getButtonData());

	if((buttonPressed & 0x01) && (moveDirection == UP || moveDirection == DOWN)) //Set new direction right only when current direction is up or down
	{
		moveDirection = RIGHT;
	}
	else if((buttonPressed & 0x02) && (moveDirection == UP || moveDirection == DOWN)) //Set new direction left when current direction is up or down
	{
		moveDirection = LEFT;
	}
	else if((buttonPressed & 0x40) && (moveDirection == LEFT || moveDirection == RIGHT)) //Set new direction up when current direction is left or right
	{
		moveDirection = UP;
	}
	else if((buttonPressed & 0x80) && (moveDirection == LEFT || moveDirection == RIGHT)) //Set new direction down when current direction is left or right
	{
		moveDirection = DOWN;
	}
	else
	{
	}

	//Call updateMove to update move direction
	updateMove();
}

//--LED & KEY-//

//--OLED-START-//
//OLED initialization
void init_Oled(void)
{
	OLED_STOPBIT;
	OLED_STARTBIT;
	OLED_sendbyte(OLED_ADDRESS+OLED_WRITE);
	OLED_sendbyte(OLED_COMMAND);
	OLED_sendbyte(OLED_ONOFF(0));
	OLED_sendbyte(OLED_DIVIDEFREQ);	OLED_sendbyte(0x80);
	OLED_sendbyte(OLED_MULTIPLEX);	OLED_sendbyte(63);
	OLED_sendbyte(OLED_OFFSET);		OLED_sendbyte(0);
	OLED_sendbyte(OLED_STARTLINE(0));
	OLED_sendbyte(OLED_CHARGEPUMP);	OLED_sendbyte(0x14);
	OLED_sendbyte(OLED_ADDRESSING);	OLED_sendbyte(OLED_HORIZONTAL); //Horizontal addressing
	OLED_sendbyte(OLED_REMAP(1));
	OLED_sendbyte(OLED_OUTDIR(1));
	OLED_sendbyte(OLED_COMPINS);		OLED_sendbyte(0x12);
	OLED_sendbyte(OLED_CONTRAST);	OLED_sendbyte(0xcf);
	OLED_sendbyte(OLED_PRECHARGE);	OLED_sendbyte(0xf1);
	OLED_sendbyte(OLED_VCOMH);		OLED_sendbyte(0x40);
	OLED_sendbyte(OLED_DISPLAYON(0));
	OLED_sendbyte(OLED_INVERSE(0));
	OLED_sendbyte(OLED_DISABLESCROLL);
	OLED_sendbyte(OLED_ONOFF(1));
	OLED_sendbyte(OLED_SETCOL);		OLED_sendbyte(0);	OLED_sendbyte(127);
	OLED_sendbyte(OLED_SETPAGE);	OLED_sendbyte(0);	OLED_sendbyte(7);
	OLED_STOPBIT;
}

//OLED layer 1
GPIO_PinState OLED_sendbyte(uint8_t data)
{
	uint8_t i;
	GPIO_PinState ack;

	//send bits from data byte
	for(i = 0; i < 8; i++)
	{
		SCL(LOW); //Pull SCL low to send data bit
		CDELAY;	//1/3 delay of clock period
		SDA(data & 0x80);	//Mask data byte and send MSB data bit
		data <<= 1;	//Shift data byte to get next bit ready for the next data bit transfer
		CDELAY;	//2/3 delay to make SDA stable
		SCL(HIGH);	//Tell to the slave to read data bit from SDA
		CDELAY;
	}

	//read SDA (ack bit)
	SCL(LOW);	//Tell to the slave to put ack bit on the SDA
	SDA(HIGH); //Release data line so slave can use SDA
	CDELAY;	//SCL delay, before SDA becomes stable
	SCL(HIGH); //Pull SCL high before reading ack bit from SDA
	ack = rSDA; //Read ack bit
	CDELAY;
	SCL(LOW); //Clear SDA from the ack bit
	CDELAY;

	return ack;
}

//Function that sends characters to the display
//char *string - string that will be sent to the display, string must contain '\0'
void OLED_printstr(char *string)
{
	int charsToSend = 0;	//Character to send
	uint8_t bytesToSend;	//Bytes to send for each character

	OLED_STARTBIT;
	OLED_sendbyte(OLED_ADDRESS+OLED_WRITE);
	OLED_sendbyte(OLED_DATA);

	while(string[charsToSend] != '\0') //Loop through string till stop bit is found
	{
		for(bytesToSend = 0; bytesToSend < 8; bytesToSend++) //Each character needs 8 bytes to be displayed
		{
			OLED_sendbyte(font[string[charsToSend]*8+bytesToSend]);	//Every 8s byte is a new character in the font
		}
		charsToSend++;
	}

	OLED_STOPBIT;
}

//Function that will clear display
void OLED_clearDisplay(void)
{
	uint8_t i;	//Amount of characters to send
	uint8_t bytesToSend;	//Amount of bytes to send

	OLED_STARTBIT;
	OLED_sendbyte(OLED_ADDRESS+OLED_WRITE);
	OLED_sendbyte(OLED_DATA);

	for(i = 0; i < 128; i++) //Send 128 characters
	{
		for(bytesToSend = 0; bytesToSend < 8; bytesToSend++) //8 Bytes for each character
		{
			OLED_sendbyte(font[82+bytesToSend]);
		}
	}
	OLED_STOPBIT; //Stop data communication

	//Start command communication
	OLED_STARTBIT;
	OLED_sendbyte(OLED_ADDRESS+OLED_WRITE); //Check if slave is online
	OLED_sendbyte(OLED_COMMAND);	//Tell the slave that command will be sent
	OLED_sendbyte(OLED_SETCOL);	//Send command to set the column followed by start and end addresses bytes
	OLED_sendbyte(0); //Column start address
	OLED_sendbyte(127); //Column end address
	OLED_sendbyte(OLED_SETPAGE);	//Send command to set page, followed by start and end addresses bytes
	OLED_sendbyte(0); //Page start address
	OLED_sendbyte(7); //Page end address
	OLED_STOPBIT;
}

//--OLEd-END-//


//--SNAKE-START-//
//Game initialization
void initGame(void)
{
	uint8_t i;
	for(i = 0; i < MaxSizeOfSnake; i++)
	{
		//Initialize snake body
		snakeBody[i] = 0;
		//Initialize snake pixels
		snakeOnScreen[i] = 0;
	}

	for(i = 0; i < 4; i++)
	{
		//Initialization of snake body position
		snakeBody[i] = headPosition-i; //First loop is head, last loop is tail
		//Initialization of snake pixels
		snakeOnScreen[i] = 0x01;
	}

	startPosition(); //Start position for snake and food
	drawGame(); //Draw frame, start position of snake and food on the OLED
	LK_displayPoints(points); //Show start points and game difficulty
}

//Function to draw the game on the OLED
void drawGame(void)
{
	int pixToDraw;

	uint8_t bytesToSend;	//Bytes to send for each character

	//Add frame to the map
	updateMap();

	OLED_STARTBIT;
	OLED_sendbyte(OLED_ADDRESS+OLED_WRITE);
	OLED_sendbyte(OLED_DATA);

	for(pixToDraw = 0; pixToDraw < 128; pixToDraw++) //Send map data to Oled
	{
		for(bytesToSend = 0; bytesToSend < 8; bytesToSend++)
		{
			OLED_sendbyte(map[pixToDraw*8+bytesToSend]);
		}
	}

	OLED_STOPBIT;
}

//Add frame on the OLED
void updateMap(void)
{
	//Add frame on the map
	uint32_t element;
	for(element = 0; element < 1024; element++)
	{
		map[element] |= frame[element];
	}
	//Add food on the map
	map[foodPosition] |= foodOnScreen;
}

//Start position for the snake and food
void startPosition(void)
{
	uint8_t i;

	//Start snake body 4 pixels length
	for(i = 0; i < 4; i++)
	{
		map[snakeBody[i]] |= snakeOnScreen[i];
	}
}

//Function that redraws snake moves
void updateMove(void)
{
	if(moveDirection == RIGHT)
	{
		rightMove();
	}
	else if(moveDirection == LEFT)
	{
		leftMove();
	}
	else if(moveDirection == UP)
	{
		upMove();
	}
	else if(moveDirection == DOWN)
	{
		downMove();
	}
}

void rightMove(void)
{
	uint8_t i;

	map[snakeBody[bodyLenth-1]] &= ~(snakeOnScreen[bodyLenth-1]); //Remove tail from the OLED

	//Update new coordinates for the snake moving to the right
	for(i = bodyLenth-1; i > 0; i--)
	{
		snakeBody[i] = snakeBody[i-1];	//Update array with new values of each snake body pixel position
		snakeOnScreen[i] = snakeOnScreen[i-1]; //Update array with new values of each snake pixel
	}
	snakeBody[0] += 1; //Add new position for the head
	snakeOnScreen[0] = 0x01; //Add new pixel for the head
	map[snakeBody[0]] |= snakeOnScreen[0]; //Add new head on the display
}

void leftMove(void)
{
	uint8_t i;

	map[snakeBody[bodyLenth-1]] &= ~(snakeOnScreen[bodyLenth-1]); //Remove tail from the OLED

	//Update new coordinates for the snake moving to the right
	for(i = bodyLenth-1; i > 0; i--)
	{
		snakeBody[i] = snakeBody[i-1];	//Update array with new values of each snake body pixel
		snakeOnScreen[i] = snakeOnScreen[i-1]; //Update array with new values of each snake pixel
	}
	snakeBody[0] -= 1; //Add new position for the head
	snakeOnScreen[0] = 0x01; //Add new pixel for the head
	map[snakeBody[0]] |= snakeOnScreen[0]; //Add new head
}

void upMove(void)
{
	uint8_t i;
	uint16_t newHeadPosition; //Help variable to jump to the next row on the display

	//Remove tail from the OLED
	map[snakeBody[bodyLenth-1]] &= ~(snakeOnScreen[bodyLenth-1]);
	moveBy = snakeOnScreen[0]; //Save current head value
	newHeadPosition = snakeBody[0] - 128; //Calculate variable for the next head position
	//Update new coordinates for the snake moving up
	for(i = bodyLenth-1; i > 0; i--)
	{
		snakeBody[i] = snakeBody[i-1];	//Update array with new values of each snake body pixel
		snakeOnScreen[i] = snakeOnScreen[i-1]; //Update array with new values of each snake pixel
	}

	if(moveBy == 0x00) //If bottom pixel is in the row is reached, reset value on the miveBy and save old head position
	{
		snakeOnScreen[0] = 0x80;
		snakeBody[0] = newHeadPosition;
	}
	else
	{
		snakeOnScreen[0] = (moveBy >> 1); //Change direction to up
	}

	map[snakeBody[0]] |= snakeOnScreen[0]; //Add new head pixel
}

void downMove(void)
{
	uint8_t i;

	//Remove tail from the OLED
	map[snakeBody[bodyLenth-1]] &= ~(snakeOnScreen[bodyLenth-1]);
	moveBy = snakeOnScreen[0]; //Save current value of head
	//Update new coordinates for the snake moving down
	for(i = bodyLenth-1; i > 0; i--)
	{
		snakeBody[i] = snakeBody[i-1];	//Update array with new values of each snake body pixel
		snakeOnScreen[i] = snakeOnScreen[i-1]; //Update array with new values of each snake pixel
	}

	if(moveBy == 128) //If bottom pixel is in the row is reached, reset value on the moveBy and save old head position
	{
		snakeOnScreen[0] = 0x01;
		snakeBody[0] += 128;
	}
	else
	{
		snakeOnScreen[0] = (moveBy << 1); //Change direction to down
	}

	map[snakeBody[0]] |= snakeOnScreen[0]; //Add new head pixel
}

//Function that checks if the snakes head is on the same position as food. Will display points and call function to add one pixel to the snake.
void checkIfFood(void)
{
	uint32_t bonusPoints = 100;

	if((snakeBody[0] == foodPosition) && (snakeOnScreen[0] == foodOnScreen))
	{
		points += (bonusPoints - gameDifficulty)/10; //Bonus for different game difficulty divided by 2 for not getting overflow of points.
		LK_displayPoints(points); //Display points on the LED & KEYs
		foodCounter++;
		addToSnake(); //Add pixel to the snake
		addNewFood();
	}
}

//Functions that is using random numbers for the position of food
void addNewFood(void)
{
	foodPosition = food[foodCounter];
	foodOnScreen = foodVal[foodCounter];
}

//Function that adds one pixel to the snakes tail, increases body length
void addToSnake(void)
{
	//Adds one pixel to the snakes tail
	snakeBody[bodyLenth] = snakeBody[bodyLenth-1];
	snakeOnScreen[bodyLenth] = snakeOnScreen[bodyLenth-1];
	bodyLenth++;
}

//Function that sets the game difficulty
void setGameDiff(void)
{
	uint8_t menuRunning = 1; //While loop flag
	uint8_t buttonPressed;  //Holds value of pressed button
	char *msgText = "Game difficulty 0 - 3(use 2 left buttons). Exit - most right button."; //Text message
	int8_t tempGameDiff = 0;


	OLED_clearDisplay();
	OLED_printstr(msgText);

	while(menuRunning)
	{
		buttonPressed = LK_decodeButtonData(LK_getButtonData());

		//Set game difficulty
		if((buttonPressed & 0x01))
		{
			if(tempGameDiff > 2)
			{
				tempGameDiff = 0;
			}
			else
			{
				tempGameDiff += 1;
			}
			HAL_Delay(100);
		}
		else if((buttonPressed & 0x02))
		{
			if(tempGameDiff < 1)
			{
				tempGameDiff = 3;
			}
			else
			{
				tempGameDiff -= 1;
			}
			HAL_Delay(100);
		}
		else if((buttonPressed & 0x80))
		{
			menuRunning = 0;
			HAL_Delay(100);
		}
		else
		{
		}

		gameDifficulty = difficulty[tempGameDiff]; //Game difficulty
		LK_displayPoints(points); //Display difficulty
		HAL_Delay(25);
	}
	OLED_clearDisplay();
	HAL_Delay(1000);
}

//Function that checks if snakes head is out of frame.
void checkOutOfFrame(void)
{

}

//--SNAKE-END-//

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler */
	/* User can add his own implementation to report the HAL error return state */
	while(1)
	{
	}
	/* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
